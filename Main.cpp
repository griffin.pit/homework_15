#include <iostream>


void ManualOddOut(int MaxIndex, bool Odd)
{
	
	for (int i = Odd; i <= MaxIndex; i+=2)
	{
		std::cout << i << ' ';
	}


}

void OddOut()
{
	for (int i = 0; i <= 10; i += 2)
	{
		std::cout << i << ' ';
	}
}

int main()
{

	std::cout << "Auto odd out: " << "\n";
	OddOut();
	
	std::cout << "\n" << "Please, enter limit number:" << "\n";
	int NumberOdd;
	std::cin >> NumberOdd;
	std::cout << "Please, enter number 0-Even; 1-Odd " << "\n";
	bool OddIn;
	std::cin >> OddIn;

	ManualOddOut(NumberOdd, OddIn);


}